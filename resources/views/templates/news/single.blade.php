@include('templates.header')
<div class="ui text container article">
    <h1 class="ui header">{{ $article['title'] }}
        <div class="ui label">
            {{ $article['date'] }}
        </div>
    </h1>
    <p>Автор: {{ $article['author'] }}</p>
    <div class="ui divider"></div>
    <div class="article-body">
        <img align="left" class="article-preview" src="/images/avatar.png">
        <p class="article-content">{{ $article['content'] }}</p>
    </div>
</div>
@include('templates.footer')
