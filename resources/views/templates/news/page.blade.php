@include('templates.header')
<section class="ui container articles">
    @foreach($articles as $article)
    <div class="ui divided items">
        <div class="item">
            <div class="image">
                <img src="/images/avatar.png">
            </div>
            <div class="content">
                <a href="/news/{{ $article['id'] }}" class="header">{{ $article['title'] }}</a>
                <div class="meta">
                    <span class="cinema">{{ $article['date'] }}</span>
                </div>
                <div class="description">
                    <p>{{ $article['excerpt'] }}</p>
                </div>
                <div class="extra">
                    <div class="ui label">{{ $article['author'] }}</div>
                </div>
            </div>
        </div>
        <div class="ui divider"></div>
    </div>
    @endforeach
</section>
@include('templates.footer')
