<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Мой блог</title>
    <link rel="stylesheet" href="/css/semantic.min.css" />
    <link rel="stylesheet" href="/css/styles.css" />
</head>
<body>
<header class="header">
    <div class="ui inverted menu">
        <div class="ui container">
            <a href="/" class="header item">Главная</a>
            <a class="item" href="/about">О нас</a>
            <a class="item" href="/news">Новости</a>
        </div>
    </div>
</header>
<main>
