@include('templates.header')
<section class="ui container about">
    <h1 class="ui header">Об агенстве новостей</h1>
    <p>Мы - ведущее информационное агенство России. 1 сентября 2022 года нам исполнилось 118 лет</p>
    <p>Мы – лидер по цитируемости среди информагентств России (по данным «Медиалогии» за 2021 год). В 2021 году агентство выпустило на всех платформах свыше 2,4 млн единиц контента.</p>
    <p>Работу ТАСС в режиме реального времени обеспечивают около 1,8 тыс. сотрудников.</p>
    <p>Региональные информационные центры в Санкт-Петербурге, Новосибирске и Екатеринбурге, десятки корпунктов в регионах России, а также 58 представительств агентства в 55 странах позволяют оперативно получать и распространять объективную информацию о событиях в России и мире среди максимально широкой аудитории.</p>
    <p>Ежедневно мы выпускаем около 3 тыс. сообщений и порядка 600-800 фото- и видеоматериалов от собственных корреспондентов в России и за рубежом, формируя целостную и объективную картину событий.</p>
    <p>Подписчикам, в числе которых российские и зарубежные СМИ, федеральные и региональные органы государственной власти, дипломатические миссии иностранных государств, бизнес-структуры и общественные организации, доступна широкая линейка из 70 новостных лент и тематических сервисов на русском, английском, испанском, французском и китайском языках.</p>
</section>
@include('templates.footer')
