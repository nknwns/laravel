<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('about');
});

Route::prefix('news')->group(function () {
    Route::get('/', function () {
        $articles = [
            ['id' => 0, 'title' => 'Ох уж эта морковь..', 'date' => '06.09.2022г', 'excerpt' => 'Изначально морковь была фиолетового цвета, но потом...', 'author' => 'Андрей', 'content' => 'Морковь — один из самых распространенных овощей по всему миру. Неприхотливая к климату, она полна витаминов, а самый популярный ее сорт отличается оранжевым цветом, по которому мы безошибочно отличаем морковь от других овощей, заполонил полки супермаркетов. Многие люди, привыкшие к стандартной морковке оранжевого цвета, удивятся, узнав о существовании других цветов. Более того, изначально этот овощ вовсе не был оранжевым: преобладали фиолетовые и желтые цвета.
            
            Дикие виды моркови существовали десятки и даже сотни тысяч лет назад, а само семейство «зонтичные», к которому относится наша героиня, существует на планете уже миллионы лет. Бесчисленные виды дикой моркови отличались размером, вкусом и цветом: от желтого до темно-фиолетового. Характерно, что более темные виды моркови произрастали в азиатском регионе, а светло-желтые оттенки были распространены в Средиземноморье.
            
            Долгое время морковь так и оставалась диким растением: она была известна человеку, но все внимание наших предков было приковано к возделыванию злаковых и немногих других одомашненных растений. До моркови руки дошли только около 10 века н.э.: ее начинают целенаправленно выращивать в Малой Азии.
            
            Спустя несколько веков морковь начинают возделывать в европейских странах. Вот только средиземноморская желтая морковь всегда оставалась невзрачным и тонким корнеплодом — не чета яркой и большой азиатской морковке. Европейцы не стали долго мириться с подобным положением дел и уже в 17 веке скрещивание сортов из разных регионов привело к появлению знакомой нам оранжевой моркови.
            
            Данный сорт корнеплода сочетает в себе полезные вещества и витамины (антоцины, каротиноиды) азиатской и средиземноморской моркови, отличается большим размером и сладким вкусом. Неудивительно, что именно оранжевая морковь завоевала популярность у покупателей. Но кое-где случается встретить и ставшую экзотической желтую или фиолетовую морковь.'],
            ['id' => 0, 'title' => 'Ох уж эта морковь..', 'date' => '06.09.2022г', 'excerpt' => 'Изначально морковь была фиолетового цвета, но потом...', 'author' => 'Андрей', 'content' => 'Морковь — один из самых распространенных овощей по всему миру. Неприхотливая к климату, она полна витаминов, а самый популярный ее сорт отличается оранжевым цветом, по которому мы безошибочно отличаем морковь от других овощей, заполонил полки супермаркетов. Многие люди, привыкшие к стандартной морковке оранжевого цвета, удивятся, узнав о существовании других цветов. Более того, изначально этот овощ вовсе не был оранжевым: преобладали фиолетовые и желтые цвета.
            
            Дикие виды моркови существовали десятки и даже сотни тысяч лет назад, а само семейство «зонтичные», к которому относится наша героиня, существует на планете уже миллионы лет. Бесчисленные виды дикой моркови отличались размером, вкусом и цветом: от желтого до темно-фиолетового. Характерно, что более темные виды моркови произрастали в азиатском регионе, а светло-желтые оттенки были распространены в Средиземноморье.
            
            Долгое время морковь так и оставалась диким растением: она была известна человеку, но все внимание наших предков было приковано к возделыванию злаковых и немногих других одомашненных растений. До моркови руки дошли только около 10 века н.э.: ее начинают целенаправленно выращивать в Малой Азии.
            
            Спустя несколько веков морковь начинают возделывать в европейских странах. Вот только средиземноморская желтая морковь всегда оставалась невзрачным и тонким корнеплодом — не чета яркой и большой азиатской морковке. Европейцы не стали долго мириться с подобным положением дел и уже в 17 веке скрещивание сортов из разных регионов привело к появлению знакомой нам оранжевой моркови.
            
            Данный сорт корнеплода сочетает в себе полезные вещества и витамины (антоцины, каротиноиды) азиатской и средиземноморской моркови, отличается большим размером и сладким вкусом. Неудивительно, что именно оранжевая морковь завоевала популярность у покупателей. Но кое-где случается встретить и ставшую экзотической желтую или фиолетовую морковь.']
        ];

        return view('templates.news.page', ['articles' => $articles]);
    });

    Route::get('/{articleID}', function ($articleID) {
        $articles = [
            ['id' => 0, 'title' => 'Ох уж эта морковь..', 'date' => '06.09.2022г', 'excerpt' => 'Изначально морковь была фиолетового цвета, но потом...', 'author' => 'Андрей', 'content' => 'Морковь — один из самых распространенных овощей по всему миру. Неприхотливая к климату, она полна витаминов, а самый популярный ее сорт отличается оранжевым цветом, по которому мы безошибочно отличаем морковь от других овощей, заполонил полки супермаркетов. Многие люди, привыкшие к стандартной морковке оранжевого цвета, удивятся, узнав о существовании других цветов. Более того, изначально этот овощ вовсе не был оранжевым: преобладали фиолетовые и желтые цвета.
            
            Дикие виды моркови существовали десятки и даже сотни тысяч лет назад, а само семейство «зонтичные», к которому относится наша героиня, существует на планете уже миллионы лет. Бесчисленные виды дикой моркови отличались размером, вкусом и цветом: от желтого до темно-фиолетового. Характерно, что более темные виды моркови произрастали в азиатском регионе, а светло-желтые оттенки были распространены в Средиземноморье.
            
            Долгое время морковь так и оставалась диким растением: она была известна человеку, но все внимание наших предков было приковано к возделыванию злаковых и немногих других одомашненных растений. До моркови руки дошли только около 10 века н.э.: ее начинают целенаправленно выращивать в Малой Азии.
            
            Спустя несколько веков морковь начинают возделывать в европейских странах. Вот только средиземноморская желтая морковь всегда оставалась невзрачным и тонким корнеплодом — не чета яркой и большой азиатской морковке. Европейцы не стали долго мириться с подобным положением дел и уже в 17 веке скрещивание сортов из разных регионов привело к появлению знакомой нам оранжевой моркови.
            
            Данный сорт корнеплода сочетает в себе полезные вещества и витамины (антоцины, каротиноиды) азиатской и средиземноморской моркови, отличается большим размером и сладким вкусом. Неудивительно, что именно оранжевая морковь завоевала популярность у покупателей. Но кое-где случается встретить и ставшую экзотической желтую или фиолетовую морковь.'],
            ['id' => 0, 'title' => 'Ох уж эта морковь..', 'date' => '06.09.2022г', 'excerpt' => 'Изначально морковь была фиолетового цвета, но потом...', 'author' => 'Андрей', 'content' => 'Морковь — один из самых распространенных овощей по всему миру. Неприхотливая к климату, она полна витаминов, а самый популярный ее сорт отличается оранжевым цветом, по которому мы безошибочно отличаем морковь от других овощей, заполонил полки супермаркетов. Многие люди, привыкшие к стандартной морковке оранжевого цвета, удивятся, узнав о существовании других цветов. Более того, изначально этот овощ вовсе не был оранжевым: преобладали фиолетовые и желтые цвета.
            
            Дикие виды моркови существовали десятки и даже сотни тысяч лет назад, а само семейство «зонтичные», к которому относится наша героиня, существует на планете уже миллионы лет. Бесчисленные виды дикой моркови отличались размером, вкусом и цветом: от желтого до темно-фиолетового. Характерно, что более темные виды моркови произрастали в азиатском регионе, а светло-желтые оттенки были распространены в Средиземноморье.
            
            Долгое время морковь так и оставалась диким растением: она была известна человеку, но все внимание наших предков было приковано к возделыванию злаковых и немногих других одомашненных растений. До моркови руки дошли только около 10 века н.э.: ее начинают целенаправленно выращивать в Малой Азии.
            
            Спустя несколько веков морковь начинают возделывать в европейских странах. Вот только средиземноморская желтая морковь всегда оставалась невзрачным и тонким корнеплодом — не чета яркой и большой азиатской морковке. Европейцы не стали долго мириться с подобным положением дел и уже в 17 веке скрещивание сортов из разных регионов привело к появлению знакомой нам оранжевой моркови.
            
            Данный сорт корнеплода сочетает в себе полезные вещества и витамины (антоцины, каротиноиды) азиатской и средиземноморской моркови, отличается большим размером и сладким вкусом. Неудивительно, что именно оранжевая морковь завоевала популярность у покупателей. Но кое-где случается встретить и ставшую экзотической желтую или фиолетовую морковь.']
        ];

        return view('templates.news.single', ['article' => $articles[$articleID]]);
    });
});
